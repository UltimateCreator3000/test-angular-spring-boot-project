import { Component, OnInit } from '@angular/core';
import {Hero} from '../../../../hero';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  powers = [ 'Really Smart', 'Super Flexible',
    'Super Hot', 'Weather Changer' ];
  model = new Hero( 18, 'Dr Strange', this.powers[ 0 ], 'Chuck Overstreet' );

  submitted = false;

  onSubmit() {this.submitted = true; }

  constructor() { }

  ngOnInit() {
  }

}
