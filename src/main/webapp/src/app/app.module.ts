import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AppComponent} from './app.component';

import {FooterComponent} from './main/components/footer/footer.component';
import {MenuComponent} from './main/components/menu/menu.component';
import {MainComponent} from './main/components/card/main/main.component';
import {TableComponent} from './main/components/card/table/table.component';
import {ImageComponent} from './main/components/card/image/image.component';
import {FormComponent} from './main/components/card/form/form.component';
import {HeaderComponent} from './main/components/header/header.component';
import {DropdownDirective} from './main/components/card/shared/dropdown.directive';
import {AuthService} from './main/components/card/auth.service';
import {SignupComponent} from './main/components/card/signup/signup.component';
import {FormsModule} from '@angular/forms';





const appRoutes: Routes = [
  { path: '', component: MainComponent },
  { path: 'table', component: TableComponent },
  { path: 'image', component: ImageComponent },
  { path: 'form', component: FormComponent },
  {path: 'signup', component: SignupComponent}

];

@NgModule( {
  declarations: [
    AppComponent,
    FooterComponent,
    MenuComponent,
    MainComponent,
    TableComponent,
    ImageComponent,
    FormComponent,
    HeaderComponent,
    DropdownDirective,
    SignupComponent
  ],

  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule

  ],
  providers: [AuthService],
  bootstrap: [ AppComponent ]
} )
export class AppModule {
}
