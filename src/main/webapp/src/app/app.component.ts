import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';
@Component( {
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
} )
export class AppComponent implements OnInit {
  title = 'my-app';

  ngOnInit() {
    firebase.initializeApp( {
      apiKey: 'AIzaSyA-uNPepgcFOuyUlJ60asGUXgTzVTJhiDY',
      authDomain: 'udemy-ng-http-7b176.firebaseapp.com'
    } );
  }
}
