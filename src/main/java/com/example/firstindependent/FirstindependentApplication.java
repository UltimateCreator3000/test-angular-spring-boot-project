package com.example.firstindependent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstindependentApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstindependentApplication.class, args);
	}
}
